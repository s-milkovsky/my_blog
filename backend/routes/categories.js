const express = require('express');
const router = express.Router();
var CategoriesModel = require('../libs/mongoose').CategoriesModel;

router.get('/', function(req, res) {
    return CategoriesModel.find(function (err, categories) {
        if (!err) {
            return res.send(categories);
        } else {
            res.statusCode = 500;
            return res.send({ error: 'Server error' });
        }
    });
});

router.post('/', function(req, res) {
    var category = new CategoriesModel({
        title: req.body.title,
        count: req.body.count
    });

    category.save(function (err) {
        if (!err) {
            return res.send({ status: 'OK', category:category });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            console.log('Internal error(%d): %s' + res.statusCode,err.message);
        }
    });
});

router.get('/:id', function(req, res) {
    return CategoriesModel.findById(req.params.id, function (err, category) {
        if(!category) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', category:category });
        } else {
            res.statusCode = 500;
            console.log('Internal error(%d): %s' + res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});


module.exports = router;
