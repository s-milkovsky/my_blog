const express = require('express');
const router = express.Router();
var PostsModel = require('../libs/mongoose').PostsModel;

router.get('/', function(req, res) {
    return PostsModel.find(function (err, posts) {
        if (!err) {
            return res.send(posts);
        } else {
            res.statusCode = 500;
            // log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.post('/', function(req, res) {
    console.log('here', req.body)
    var post = new PostsModel({
        title: req.body.title,
        date: req.body.date,
        email: req.body.email,  
        type: req.body.type,  
        author: req.body.author,   
        description: req.body.description
    });

    post.save(function (err) {
        if (!err) {
            return res.send({ status: 'OK', post:post });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            console.log('Internal error(%d): %s' + res.statusCode,err.message);
        }
    });
});

router.get('/:id', function(req, res) {
    return PostsModel.findById(req.params.id, function (err, post) {
        if(!post) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send(post);
        } else {
            res.statusCode = 500;
            console.log('Internal error(%d): %s' + res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.put('/:id', function (req, res){
    return PostsModel.findById(req.params.id, function (err, post) {
        if(!post) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        post.title = req.body.title;
        post.date = req.body.date;
        post.description = req.body.description;
        post.comments = req.body.comments;
        
        return post.save(function (err) {
            if (!err) {
                console.log("post updated");
                return res.send({ status: 'OK', post:post });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                console.log('Internal error(%d): %s' + res.statusCode,err.message);
            }
        });
    });
});

router.delete('/:id', function (req, res){
    return PostsModel.findById(req.params.id, function (err, post) {
        if(!post) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return post.remove(function (err) {
            if (!err) {
                console.log("post removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                console.log('Internal error(%d): %s' + res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    });
});




module.exports = router;
