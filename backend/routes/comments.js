const express = require('express');
const router = express.Router();
var PostsModel = require('../libs/mongoose').PostsModel;
var CommentsModel = require('../libs/mongoose').CommentsModel;

router.post('/', function(req, res) {
    console.log('comment', req.body)
    var comment = new CommentsModel({
        postId: req.body.postId,
        postType: req.body.postType,
        email: req.body.email,  
        author: req.body.author,   
        description: req.body.description
    });

    comment.save(function (err) {
        if (!err) {
            return res.send({ status: 'OK', comment:comment });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            console.log('Internal error(%d): %s' + res.statusCode,err.message);
        }
    });
});

router.get('/:id', function (req, res) {
    PostsModel.findOne({ _id: req.params.id }, function (err, post) {
        CommentsModel.find({  }, (err, comments) => { // postId: post._id
            if (!err) {
                return res.send(comments);
            } else {
                res.statusCode = 500;
                return res.send({ error: 'Server error' });
            }

        })
    });
});

module.exports = router;
