var mongoose    = require('mongoose');

mongoose.connect('mongodb://localhost/blogDB');
var db = mongoose.connection;

db.on('error', function (err) {
    // log.error('connection error:', err.message);
});
db.once('open', function callback () {
    // log.info("Connected to DB!");
    console.log("Connected to DB!");
});

var Schema = mongoose.Schema;

// Schemas

var Comments = new Schema({
    postId: { type: String, required: true },
    postType: { type: String, required: true },
    author: { type: String, required: true },
    email: { type: String, required: true },
    description: { type: String, required: true },
    date: { type: Date, default: Date.now }
});

var Posts = new Schema({
    title: { type: String, required: true },
    date: { type: Date, default: Date.now },
    email: { type: String, required: true },
    type: {type: String, required: true},
    author: {type: String, required: true},
    description: { type: String, required: true }
});

var Categories = new Schema({
    title: { type: String, required: true },
    count: { type: Number, required: true }
});


// validation

// Posts.path('title').validate(function (v) {
//     return v.length > 5 && v.length < 70;
// });

var PostsModel = mongoose.model('Posts', Posts);
var CommentsModel = mongoose.model('Comments', Comments);
var CategoriesModel = mongoose.model('Categories', Categories);

module.exports.PostsModel = PostsModel;
module.exports.CommentsModel = CommentsModel;
module.exports.CategoriesModel = CategoriesModel;