# README #

* Склонировать прoект:

```
#!html

https://s-milkovsky@bitbucket.org/s-milkovsky/my_blog.git
```
если у вас установлены SSH ключи то можно через:

```
#!html

git@bitbucket.org:s-milkovsky/my_blog.git
```

* Зайти в папку **/backend** открыть терминал и запустить **npm install** теже действия проделать в папке **/front**
* Утсановить mongo если нету ([https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/](Link URL)) 
* запустить **sudo service mongod start**
* путь к базе **mongod --path BLOG_project/backend/data/**

* Перейти в папку **/backend** и из нового терминала запустить ноду **npm start** . В терминале появиться текст ниже

```
#!html

> application-name@0.0.1 start /home/student/BLOG_project/backend
> nodejs ./bin/www
 
Connected to DB!
```

значит **mongo** работает

* перейти в папку **/front**, открыть новый терминал и запустить команду **webpack**
* после того как она отработает в этом же терминале запустить **webpack-dev-server**

* Открываем браузер и смотрим [localhost:8080](Link URL)