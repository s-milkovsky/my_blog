import ng from 'angular';
import uiRouter from 'angular-ui-router';

import postsWrapper from './components/posts';
import post from './components/posts.item';
import sidebarWrapper from './components/sidebar';
import item from './components/sidebar.item';
import homePage from './components/homePage';
import createPost from './components/createPost';
import postDetails from './components/postDetails';
import postComment from './components/postDetails.comment';
import './less/style.less';

ng.module('app', [postsWrapper, post, sidebarWrapper, item, homePage, createPost, postDetails, postComment, uiRouter])
       .config(($locationProvider, $stateProvider) => {
            'ngInject';
            $locationProvider.html5Mode(true);
            const states = [
                {
                    name: 'home',
                    url: '/',
                    component: 'homePage'
                },
                {
                    name: 'create',
                    url: '/createPost',
                    component: 'createPost'
                },
                {
                    name: 'detail',
                    url: '/postDetails',
                    component: 'postDetails'
                }
                
            ];
            states.forEach(state => $stateProvider.state(state));
        });