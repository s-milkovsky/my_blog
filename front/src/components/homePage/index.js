import ng from 'angular';
import "bootstrap/less/bootstrap.less";

import homePageComponent from './homePage.component';

export default ng.module('homePage.component', [])
    .component('homePage', homePageComponent)
    .name;