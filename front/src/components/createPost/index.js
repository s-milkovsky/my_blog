import ng from 'angular';
import "bootstrap/less/bootstrap.less";

import createPostComponent from './createPost.component';
import postsService from '../posts/posts.service';

export default ng.module('createPost.component', [])
    .component('createPost', createPostComponent)
    .service('postsService', postsService)
    .name;