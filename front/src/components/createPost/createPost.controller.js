export default class createPostController {
  constructor(postsService) {
    'ngInject';
    this.postsService = postsService;
  }
  $onInit() {
    
  }

  createPost() {
    const newPost = {
      title: this.title,
      date: Date.now(),
      email: this.email,
      type: this.type,
      author: this.author,
      description: this.description
    }
    this.postsService.add(newPost);
    
    this.title = '';
    this.date = '';
    this.type = '';
    this.description = '';
    this.author = '';
  }

}