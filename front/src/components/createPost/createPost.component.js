import template from './createPost.template.html';
import controller from './createPost.controller';

export default {
    template,
    controller
};