import template from './sidebar.item.template.html';
import controller from './sidebar.item.controller';

export default {
    template,
    controller,
    bindings: {
        currentItem: '<'
    }
};