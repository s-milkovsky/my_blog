import ng from 'angular';
import sidebardItemComponent from './sidebar.item.component';
import sidebarService from '../sidebar/sidebar.service';

export default ng.module('sidebarItem.component', [])
    .component('item', sidebardItemComponent)
    .service('sidebarService', sidebarService)
    .name;