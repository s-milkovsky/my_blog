import template from './post.details.cmment.template.html';
import controller from './post.details.comment.controller';

export default {
    template,
    controller,
    bindings: {
        currentComment: '<'
    }
}
