import ng from 'angular';

import postDetailsCommentComponent from './post.details.comment.component';

export default ng.module('post.details.comment.component', [])
    .component('comment', postDetailsCommentComponent)
    .name;