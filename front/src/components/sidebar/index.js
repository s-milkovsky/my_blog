import ng from 'angular';
import "bootstrap/less/bootstrap.less";

import sidebarComponent from './sidebar.component';
import categoriesService from './sidebar.service';
import postsService from '../posts/posts.service';

export default ng.module('sidebar.component', [])
    .component('sidebarWrapper', sidebarComponent)
    .service('categoriesService', categoriesService)
    .name;