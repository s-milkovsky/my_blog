import template from './sidebar.template.html';
import controller from './sidebar.controller';
import style from './sidebar.less';

export default {
    template,
    controller,
    style
};