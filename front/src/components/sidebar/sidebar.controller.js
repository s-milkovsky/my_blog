export default class sidebarController {
  constructor(sidebarService, postsService) {
    'ngInject';
    this.sidebarService = sidebarService;
    this.postsService = postsService;
  }
  $onInit() {
    this.init();
    
  }

  init() {
    this.sidebarService.list().then(items => {
      this.items = items;
    })
    this.postsService.list().then(posts => {
      this.posts = posts;
      
    })
  }
}