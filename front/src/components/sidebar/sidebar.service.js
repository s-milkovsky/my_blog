export default class categoriesService{
    constructor($http){
        'ngInject';
        this.$http = $http;
        this.URL = 'http://localhost:3000';
    }

    list(){
        return this.$http.get(`${this.URL}/categories/`).then(result => result.data);
    }

    detail(id){
        return this.$http.get(`${this.URL}/categories/${id}`).then(result => result.data.todos);
    }

    update(data){
        return this.$http.put(`${this.URL}/categories/${data.id}`, data).then(result => result.data);
    }

    remove(categorieId){
        return this.$http.delete(`${this.URL}/categories/${categorieId}`);
    }

    
    add(data){
        data.isComplete = false;
        return this.$http.post(`${this.URL}/categories`, data).then(result => result.data);
    }
    
    save(data){
        if(data.id){
            return this.update(data);
        }
        return this.add(data);
    }
}