export default class postsDetailsController {
  constructor(postDetailsService) {
    'ngInject';
    this.postDetailsService = postDetailsService;
  }
  $onInit() {
    this.init();
    this.comments = [];
  }

  init(){

    var postId = localStorage.getItem('postId');
    var postType = localStorage.getItem('postType');
    this.postDetailsService.detail(postId).then(post => {
      const formatDay = `${new Date(post.date).getDate()}/${new Date(post.date).getMonth()}/${new Date(post.date).getFullYear()}`;

      this.post = post;
      post.date = formatDay;

      

      return this.post;
    }).then(postId => {
      this.postDetailsService.comentsDetail(postId).then(comments => {
        comments.forEach(comment => {
          if(comment.postType === this.post.type){
            const currentDay = new Date(Date.parse(comment.date));
            comment.date = `${currentDay.getDate()}/${currentDay.getMonth()}/${currentDay.getFullYear()} - ${currentDay.getHours()}:${currentDay.getMinutes()}`;
            this.comments.push(comment);
          }
          
        })
      })
    });

  }
  addComment() {

    const newComent = {
      postId: this.post._id,
      postType: this.post.type,
      author: this.author,
      email: this.email,
      description: this.description,
      date: Date.now()
    }
    this.postDetailsService.add(newComent).then(this.init.bind(this));

    this.author = '';
    this.email = '';
    this.description = '';
    // localStorage.removeItem('postId');
    // localStorage.removeItem('postType');
  }

}