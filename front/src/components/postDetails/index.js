import ng from 'angular';

import postDetailsComponent from './post.details.component';
import postDetailsService from './post.details.service'
import "bootstrap/less/bootstrap.less";

export default ng.module('postDetails.component', [])
    .component('postDetails', postDetailsComponent)
    .service('postDetailsService', postDetailsService)
    .name;