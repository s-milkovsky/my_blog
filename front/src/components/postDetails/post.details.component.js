import template from './post.details.template.html';
import controller from './post.details.controller';

export default {
    template,
    controller
}