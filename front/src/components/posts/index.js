import ng from 'angular';
import "bootstrap/less/bootstrap.less";

import postsWrapperComponent from './posts.component';
import postsService from './posts.service';

export default ng.module('postsWrapper.component', [])
    .component('postsWrapper', postsWrapperComponent)
    .service('postsService', postsService)
    .name;