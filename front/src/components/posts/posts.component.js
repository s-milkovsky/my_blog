import template from './posts.template.html';
import controller from './posts.controller';
import style from './posts.style.less';

export default {
    template,
    controller,
    style
};