export default class postsWrapperController {
  constructor(postsService) {
    'ngInject';
    this.postsService = postsService;
  }
  $onInit() {
    this.init();
  }

  init() {
    this.postsService.list().then(posts => {
      this.posts = posts;
      
    }).then(() => this.posts.forEach((post) => {
      const formatDay = `${new Date(post.date).getDate()}/${new Date(post.date).getMonth()}/${new Date(post.date).getFullYear()} - ${new Date(post.date).getHours()}:${new Date(post.date).getMinutes()}`;
      post.date = formatDay;

      function catString(str, maxlength) {
        if(str.length < maxlength){
          return str;
        }else{
          var strCat = str.substr(0, maxlength - 3);
          return strCat = strCat + '...';
        }
      }

      const res = catString(post.description, 300);
      post.description = res;

    }));
  }

  getComments(id) {
    this.postsService.detail(id); //  <--> Comments service
  }
  

}