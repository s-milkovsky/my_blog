export default class postsService{
    constructor($http){
        'ngInject';
        this.$http = $http;
        this.URL = 'http://localhost:3000';
    }

    list(){
        return this.$http.get(`${this.URL}/posts`).then(result => result.data);
    }

    detail(id){
        return this.$http.get(`${this.URL}/posts/${id}`).then(result => result.data);
    }

    update(data){
        // return this.$http.put(`${this.URL}/posts/${data.id}`, data).then(result => result.data);
    }

    remove(postId){
        // return this.$http.delete(`${this.URL}/posts/${postId}`);
    }

    
    add(data){
        return this.$http.post(`${this.URL}/posts`, data).then(result => result.data);
    }
    
    save(data){
        if(data.id){
            return this.update(data);
        }
        return this.add(data);
    }
}