export default class postController {
  constructor(postsService) {
    'ngInject';
    this.postsService = postsService;
  }
  $onInit() {
    this.init();
  }

  init() {
  }

  savePostId(){
      localStorage.setItem('postId', this.currentPost._id);
      localStorage.setItem('postType', this.currentPost.type);
  }
}