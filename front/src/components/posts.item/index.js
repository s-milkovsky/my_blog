import ng from 'angular';

import postListComponent from './post.component';
import postsService from '../posts/posts.service';

export default ng.module('postItem.component', [])
    .component('post', postListComponent)
    .service('postsService', postsService)
    .name;